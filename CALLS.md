## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Clearcable NOMS. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Clearcable NOMS.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Clearcable Network Operation and Management. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getCableModemRealTimeData(macAddress, callback)</td>
    <td style="padding:15px">Get Cable Modem Real Time Data</td>
    <td style="padding:15px">{base_path}/{version}/read/realtimedata/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMacsWithHourlyData(callback)</td>
    <td style="padding:15px">Get All MACs with Hourly Data</td>
    <td style="padding:15px">{base_path}/{version}/read/cablemodem_transfer/v0.1/list_macs/hourly?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMacWithHourlyData(macAddress, callback)</td>
    <td style="padding:15px">Get MAC with Hourly Data</td>
    <td style="padding:15px">{base_path}/{version}/read/cablemodem_transfer/v0.1/hourly/mac/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMacWithMonthlyData(macAddress, callback)</td>
    <td style="padding:15px">Get MAC with Monthly Data</td>
    <td style="padding:15px">{base_path}/{version}/read/cablemodem_transfer/v0.1/monthly/mac/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSumOfTransfersInCurrentMonth(macAddress, callback)</td>
    <td style="padding:15px">Get Sum of Transfers Current Month</td>
    <td style="padding:15px">{base_path}/{version}/read/cablemodem_transfer/v0.1/this_month_sum/mac/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMACsTransferredMostCurrentMonth(limit, callback)</td>
    <td style="padding:15px">Get MACs Transferred Most Current Month</td>
    <td style="padding:15px">{base_path}/{version}/read/cablemodem_transfer/v0.1/top_this_month/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSumTransfersInMonthForEachMac(date, callback)</td>
    <td style="padding:15px">Get Sum of Transfers by Month</td>
    <td style="padding:15px">{base_path}/{version}/read/cablemodem_transfer/v0.1/sum_per_mac_in_month/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSumTransfersInMonthForEachMacCount(date, callback)</td>
    <td style="padding:15px">Get Sum of Transfers by Month Count</td>
    <td style="padding:15px">{base_path}/{version}/read/cablemodem_transfer/v0.1/sum_per_mac_in_month/{pathv1}/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSumTransfersInMonthForEachMacSortTotal(date, callback)</td>
    <td style="padding:15px">Get Sum of Transfers by Month Sort Total</td>
    <td style="padding:15px">{base_path}/{version}/read/cablemodem_transfer/v0.1/sum_per_mac_in_month/{pathv1}/sort/total?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSumTransfersInMonthForEachMacSortUpload(date, callback)</td>
    <td style="padding:15px">Get Sum of Transfers by Month Sort Upload</td>
    <td style="padding:15px">{base_path}/{version}/read/cablemodem_transfer/v0.1/sum_per_mac_in_month/{pathv1}/sort/upload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSumTransfersInMonthForEachMacSortDownload(date, callback)</td>
    <td style="padding:15px">Get Sum of Transfers by Month Sort Download</td>
    <td style="padding:15px">{base_path}/{version}/read/cablemodem_transfer/v0.1/sum_per_mac_in_month/{pathv1}/sort/download?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSumTransfersInMonthForEachMacLimit(date, limitStart, limitCount, callback)</td>
    <td style="padding:15px">Get Sum of Transfers by Month Limit</td>
    <td style="padding:15px">{base_path}/{version}/read/cablemodem_transfer/v0.1/sum_per_mac_in_month/{pathv1}/limit/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSumServices(date, callback)</td>
    <td style="padding:15px">Get Services by Month</td>
    <td style="padding:15px">{base_path}/{version}/read/cablemodem_transfer/v0.1/list_services_in_month/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllCableModemHourly(callback)</td>
    <td style="padding:15px">Get All Cable Modems Hourly</td>
    <td style="padding:15px">{base_path}/{version}/read/cablemodem_rf/v0.1/list_macs/hourly?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCableModemHourly(macAddress, callback)</td>
    <td style="padding:15px">Get Cable Modem Hourly</td>
    <td style="padding:15px">{base_path}/{version}/read/cablemodem_rf/v0.1/hourly/mac/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCableModemDailyMac(macAddress, callback)</td>
    <td style="padding:15px">Get Cable Modem Daily</td>
    <td style="padding:15px">{base_path}/{version}/read/cablemodem_rf/v0.1/daily/mac/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCableModemMonthly(macAddress, callback)</td>
    <td style="padding:15px">Get Cable Modem Monthly</td>
    <td style="padding:15px">{base_path}/{version}/read/cablemodem_rf/v0.1/monthly/mac/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCableModemCurrentMonthAverage(macAddress, callback)</td>
    <td style="padding:15px">Get Cable Modem Current Month Average</td>
    <td style="padding:15px">{base_path}/{version}/read/cablemodem_rf/v0.1/this_month_average/mac/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCableModemAverageMac(date, callback)</td>
    <td style="padding:15px">Get Cable Modem Average MAC</td>
    <td style="padding:15px">{base_path}/{version}/read/cablemodem_transfer/v0.1/average_per_mac_in_month/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllCableModemFlapHourly(callback)</td>
    <td style="padding:15px">Get All Cable Modem Flaps Hourly</td>
    <td style="padding:15px">{base_path}/{version}/read/cablemodem_flap/v0.1/list_macs/hourly?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCableModemFlapHourly(macAddress, callback)</td>
    <td style="padding:15px">Get Cable Modem Flaps Hourly</td>
    <td style="padding:15px">{base_path}/{version}/read/cablemodem_flap/v0.1/list_macs/hourly/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCableModemFlapDaily(macAddress, callback)</td>
    <td style="padding:15px">Get Cable Modem Flaps Daily</td>
    <td style="padding:15px">{base_path}/{version}/read/cablemodem_flap/v0.1/daily/mac/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCableModemFlapMonthly(macAddress, callback)</td>
    <td style="padding:15px">Get Cable Modem Flaps Monthly</td>
    <td style="padding:15px">{base_path}/{version}/read/cablemodem_flap/v0.1/monthly/mac/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCableModemFlapCurrentDay(macAddress, callback)</td>
    <td style="padding:15px">Get Cable Modem Flaps Current Day</td>
    <td style="padding:15px">{base_path}/{version}/read/cablemodem_flap/v0.1/this_day_sum/mac/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCableModemFlapCurrentMonth(macAddress, callback)</td>
    <td style="padding:15px">Get Cable Modem Flaps Current Month</td>
    <td style="padding:15px">{base_path}/{version}/read/cablemodem_flap/v0.1/this_month_sum/mac/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCableModemFlapLastMonth(macAddress, callback)</td>
    <td style="padding:15px">Get Cable Modem Flaps Last Month</td>
    <td style="padding:15px">{base_path}/{version}/read/cablemodem_flap/v0.1/last_month_sum/mac/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCableModemFlapCurrentMonthMost(limit, callback)</td>
    <td style="padding:15px">Get Cable Modem Flaps Current Month Most</td>
    <td style="padding:15px">{base_path}/{version}/read/cablemodem_flap/v0.1/top_this_hour/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCableModemStatus(macAddress, callback)</td>
    <td style="padding:15px">Get Cable Modem Status</td>
    <td style="padding:15px">{base_path}/{version}/read/cablemodem_status/v0.1/mac/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
