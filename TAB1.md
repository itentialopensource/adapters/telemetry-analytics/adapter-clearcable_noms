# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Clearcable_noms System. The API that was used to build the adapter for Clearcable_noms is usually available in the report directory of this adapter. The adapter utilizes the Clearcable_noms API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Clearcable NOMS adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Clearcable NOMS. 
With this adapter you have the ability to perform operations with Clearcable NOMS such as:

- Get Cable Modem Data

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
