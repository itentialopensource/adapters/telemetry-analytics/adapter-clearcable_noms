
## 0.5.4 [10-15-2024]

* Changes made at 2024.10.14_20:24PM

See merge request itentialopensource/adapters/adapter-clearcable_noms!12

---

## 0.5.3 [08-24-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-clearcable_noms!10

---

## 0.5.2 [08-14-2024]

* Changes made at 2024.08.14_18:36PM

See merge request itentialopensource/adapters/adapter-clearcable_noms!9

---

## 0.5.1 [08-07-2024]

* Changes made at 2024.08.06_19:48PM

See merge request itentialopensource/adapters/adapter-clearcable_noms!8

---

## 0.5.0 [05-20-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/telemetry-analytics/adapter-clearcable_noms!7

---

## 0.4.3 [03-27-2024]

* Changes made at 2024.03.27_13:14PM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-clearcable_noms!6

---

## 0.4.2 [03-12-2024]

* Changes made at 2024.03.12_10:59AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-clearcable_noms!5

---

## 0.4.1 [02-27-2024]

* Changes made at 2024.02.27_11:33AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-clearcable_noms!4

---

## 0.4.0 [01-05-2024]

* Minor/2023 auto migration

See merge request itentialopensource/adapters/telemetry-analytics/adapter-clearcable_noms!3

---

## 0.3.0 [01-03-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/telemetry-analytics/adapter-clearcable_noms!2

---

## 0.2.0 [05-27-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-clearcable_noms!1

---

## 0.1.1 [07-29-2021]

- Initial Commit

See commit afa0d5d

---
