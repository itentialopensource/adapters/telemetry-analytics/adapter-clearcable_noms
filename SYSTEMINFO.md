# Clearcable NOMS

Vendor: Clearcable
Homepage: https://www.clearcable.ca/

Product: NOMS
Product Page: https://www.clearcable.ca/

## Introduction
We classify Clearcable NOMS into the Service Assurance domain as Clearcable NOMS is an operational management platform built to support high speed data and voice service providers.

## Why Integrate
The Clearcable NOMS adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Clearcable NOMS. 
With this adapter you have the ability to perform operations with Clearcable NOMS such as:

- Get Cable Modem Data

## Additional Product Documentation

